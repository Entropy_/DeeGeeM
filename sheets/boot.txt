# Welcome to Sonic Pi v2.9
with_synth :supersaw do
  play 62, pan: -1, amp: 0.5
  sleep 0.50
  play 64, pan: 1, amp: 0.75
  sleep 0.50
  play 66, amp: 1
end