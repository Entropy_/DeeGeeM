# Welcome to Sonic Pi v2.9
#DeeGee 'awesome' sound
with_synth :supersaw do
  play_chord (chord_degree 1, :c, :major, 3, invert: 3)
  sleep 0.25
  play_chord (chord_degree 1, :g, :major, 3, invert: 3)
end